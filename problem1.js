
const fs = require('fs')
const path = require('path')

function createAndDeleteRandomFiles() {
    fs.mkdir('randomJsonFiles', (error) => {
        if (error) {
            console.log(error)
        }
        console.log("new Directory created")
    })

    const fileNamesArray = ["file1.json", "file2.json"]

    fileNamesArray.forEach((fileName) => {
        fs.writeFile(path.join("randomJsonFiles", fileName), `im in ${fileName}`, (error) => {
            if (error) { throw error }
            console.log(`${fileName} created `)
        })
    })

    fs.readdir("randomJsonFiles", (error, files) => {
        if (error) { console.log(error) }

        files.forEach((file) => {
            fs.unlink(path.join("randomJsonFiles", file), (e) => {
                if (e) { console.log(e) }
                console.log(`${file} deleted`)
            })
        })
    })


}

module.exports.createAndDeleteRandomFiles = createAndDeleteRandomFiles
