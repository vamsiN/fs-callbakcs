const fs = require('fs')
const path = require('path')
const fsp = require('fs').promises;

function operationsOnLipsumFile() {
    try {

        fs.readFile('../lipsum.txt', "UTF-8", ((error, data) => {
            if (error) { throw error }
            let dataInUpperCase = data.toUpperCase()
            console.log(`step 1, content in lipsum.txt converted to UpperCase`)

            fs.writeFile("upCaseText.txt", dataInUpperCase, ((error) => {
                if (error) { throw error }
                console.log(`step 2, upperCase content stored into upCaseText.txt`)
            }))
        }))


        fs.writeFile("filenames.txt", "upCaseText.txt", ((error) => {
            if (error) { throw error }
            console.log("step 3, stored the newly created file name in filenames.txt")
        }))

        fs.readFile('upCaseText.txt', "UTF-8", (error, data) => {
            //console.log(data)
            let dataInLowerCase = data.toLowerCase()
            //console.log(dataInLowerCase)
            console.log(`step4, content converted to lowerCase`)

            let splittedSentencesArray = dataInLowerCase.split(".")

            splittedSentencesArray.forEach((each) => {
                let eachLine = each += "\n"
                fs.appendFile("lowerCaseText.txt", eachLine, ((error) => {
                    if (error) { throw error }
                }))

            })
            console.log(`step5, lowerCase data stored to LowerCaseText.txt`)
        })

        fs.appendFile("filenames.txt", "\n" + "lowerCaseText.txt", ((error) => {
            if (error) { throw error }
            console.log(`step6, stored lowerCaseText.txt file name in filenames.txt`)
        }))


        fs.readFile('./filenames.txt', 'utf-8', (err, file) => {
            if (err) { console.log(err) }
            //console.log(file)
            const lines = file.split('\n')

            const contentArray = lines.reduce((acc, each) => {
                acc.push(fs.readFileSync(each, 'utf8'))
                return acc
            }, [])

            console.log(`step 7, all the content stored into an array to sort`)

            const sortedContentArray = contentArray.sort((a, b) => {
                return a - b
            })

            

            sortedContentArray.forEach((each) => {
                fs.appendFile('output.txt', each, (error) => {
                    if (error) { console.log(error) }

                })
            })
            console.log('step 8, sorted content stored in output.txt file')

            fs.appendFile('filenames.txt', '\n'+'output.txt',((error)=>{
                if(error){throw error}
                console.log(`step 9 output.txt file name added to filenames.txt file`)
            }))
        });

        fs.readFile('filenames.txt','utf8',((error,data)=>{
            if(error){throw error}
            const fileNamesArray = data.split('\n')
            fileNamesArray.forEach((each)=>{
                fs.unlink(each,((error)=>{
                    if(error){throw error}
                    console.log(`deleting ${each} file`)
                }))
            })
            console.log(`step 10, deleted all the files that are listed in filenames.txt file`)
        }))



    } catch (error) {
        console.log(error)
    }

}

module.exports = operationsOnLipsumFile




